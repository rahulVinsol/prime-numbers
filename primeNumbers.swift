func printPrime(count: Int) {
	if count <= 0 || count > 100000 {
		return
	}
	var primeNumbersCount = 0
	var currentPrimeNumber = 2
	var primeNumberArr = [Int]()
	while(primeNumbersCount < count) {
		var root = Double(currentPrimeNumber).squareRoot()
		root.round()
		
		var isPrime = true
		
		for divisor in primeNumberArr {
			if divisor > Int(root) {
				break
			}
			if currentPrimeNumber%divisor == 0 {
				isPrime = false
				break
			}
		}
		
		if isPrime {
			primeNumberArr.append(currentPrimeNumber)
			print(currentPrimeNumber)
			primeNumbersCount+=1
		}
		
		currentPrimeNumber+=1
	}
}
